/* 
	Найдем последний вход "опаздуна" на текущую дату
	и обновим его текущим "корректным" временим
*/
SET DATEFORMAT YMD

DECLARE
@i int = 0,
@result varchar(150),
@currentDate DATETIME = GETDATE(),
@currentDateWithoutTime varchar(50),
@correctDate varchar(50),   /* время входа */
@correctDate2 varchar(50),    /* время входа + 2 сек.*/
@ID_HozOrgan int = 735, /* HozOrgan 735 kms */
@minute varchar(2) = FLOOR(RAND()*(28-10)+10),
@seconds int = FLOOR(RAND()*(59-10)+10),
@seconds2 int;

SET @seconds2 = @seconds+1;
SET @currentDateWithoutTime = CAST(@currentDate AS Date);
SET @currentDate = @currentDateWithoutTime+' 08:30:00.00';
SET @correctDate = @currentDateWithoutTime+' 08:'+@minute+':'+CAST(@seconds AS VARCHAR(2))+'.000';
SET @correctDate2 = @currentDateWithoutTime+' 08:'+@minute+':'+CAST(@seconds+1 AS VARCHAR(2))+'.000';

/*PRINT @currentDate;
PRINT @correctDate;
PRINT @correctDate2;*/

/*RETURN;*/
WHILE @i < 2
BEGIN
	IF @i = 1
	BEGIN
		SET @correctDate = @correctDate2;
	END	

	PRINT @correctDate;
	
	SELECT TOP 1 CAST(TimeVal AS time) AS TimeValT ,CAST(DeviceTime AS time) AS DeviceTimeT, TimeVal, DeviceTime, GUID, Mode
	  FROM [orion1208].[dbo].[pLogData]
	  WHERE [HozOrgan] = @ID_HozOrgan AND [Mode]='1' AND DeviceTime > @currentDate
   
	UPDATE TOP(1) [orion1208].[dbo].[pLogData]
	   SET [TimeVal] = @correctDate, [DeviceTime] = @correctDate
	   WHERE [HozOrgan]=@ID_HozOrgan AND [Mode]='1' AND DeviceTime > @currentDate
	
	SET @i = @i + 1;
END

GO




