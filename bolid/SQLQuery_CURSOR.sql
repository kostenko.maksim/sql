DECLARE @ID INT
DECLARE @OwnerName VARCHAR (500)
DECLARE @Finish VARCHAR (500)
DECLARE @Start VARCHAR (500)

SET DATEFORMAT YMD;
/*Объявляем курсор*/
DECLARE @CURSOR CURSOR

/*Заполняем курсор*/
SET @CURSOR  = CURSOR SCROLL
FOR
SELECT ID,OwnerName,Start,Finish
FROM [orion1208].[dbo].[pMark]
WHERE CAST(Finish AS date) = '2020-12-31 00:00:00.000'

/*Открываем курсор*/

    OPEN @CURSOR
/*Выбираем первую строку*/
    FETCH NEXT FROM @CURSOR INTO @ID, @OwnerName, @Start, @Finish
/*Выполняем в цикле перебор строк*/
    WHILE @@FETCH_STATUS = 0
BEGIN

/*Вставляем параметры в третью таблицу если условие соблюдается
INSERT INTO My_Third_Table (VALUE, NAME) VALUE(@VAL, @NAM)*/
UPDATE [orion1208].[dbo].[pMark]
SET [Finish] = '2050-12-31 00:00:00.000'
WHERE [ID]= @ID
    PRINT 'ID => '+CAST(@ID AS VARCHAR(500)) + ' || Finish => '+CAST(@Finish AS VARCHAR(500));

/*Выбираем следующую строку*/
FETCH NEXT FROM @CURSOR INTO @ID, @OwnerName, @Start, @Finish
END
CLOSE @CURSOR