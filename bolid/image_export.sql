CREATE PROCEDURE [dbo].[ExportImages]
( @ID int )
AS
BEGIN
   DECLARE @ImageData VARBINARY (max)
   , @Path2OutFile NVARCHAR (2000)
   , @Obj INT 
   , @fio NVARCHAR (2000)
   , @i bigint;
 
   --SET NOCOUNT ON
   DECLARE @Doctable TABLE (id int identity(1,1), [Name] varchar(25),[FirstName] varchar(25),[MidName] varchar(25), [Picture] varBinary(max) );

INSERT INTO @Doctable([Name],[FirstName],[MidName],[Picture])
SELECT Name,FirstName,MidName,Picture
FROM [orion].[dbo].[pList]
WHERE [MidName] LIKE '%ич';

SELECT @i = COUNT(*) FROM @Doctable

                              WHILE @i >= 1

BEGIN

SELECT
        @ImageData = [Picture],
    @fio = [Name]+' '+[FirstName]+' '+[MidName],
    @Path2OutFile = 'c:\temp\photo\1\'+@fio+'.jpg'

FROM @Doctable WHERE id = @i
--print @fio
/*SELECT @ImageData = convert (VARBINARY (max), Picture, 1),@fio = Name+' '+FirstName+' '+MidName
FROM [orion].[dbo].[pList]
WHERE [MidName] LIKE '%ич';

 SET @Path2OutFile = 'c:\temp\photo\1\'+@fio+'.jpg';  */

BEGIN TRY
EXEC sp_OACreate 'ADODB.Stream' ,@Obj OUTPUT;
EXEC sp_OASetProperty @Obj ,'Type',1;
EXEC sp_OAMethod @Obj,'Open';
EXEC sp_OAMethod @Obj,'Write', NULL, @ImageData;
EXEC sp_OAMethod @Obj,'SaveToFile', NULL, @Path2OutFile, 2;
EXEC sp_OAMethod @Obj,'Close';
EXEC sp_OADestroy @Obj;
END TRY

BEGIN CATCH
EXEC sp_OADestroy @Obj;
END CATCH
END
  /*SET NOCOUNT OFF*/
/*END
GO*/

